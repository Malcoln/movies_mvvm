package movies.mvvm.hotel.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import movies.mvvm.hotel.model.Hotel
import movies.mvvm.hotel.repository.HotelRepository

class HotelDetailsViewModel(
    private val repository: HotelRepository
) : ViewModel() {
    fun loadHotelDetails(id: Long): LiveData<Hotel> {
        return repository.hotelById(id)
    }
}
