package movies.mvvm.hotel.di

import movies.mvvm.hotel.details.HotelDetailsViewModel
import movies.mvvm.hotel.form.HotelFormViewModel
import movies.mvvm.hotel.list.HotelListViewModel
import movies.mvvm.hotel.repository.HotelRepository
import movies.mvvm.hotel.repository.room.HotelDatabase
import movies.mvvm.hotel.repository.room.RoomRepository
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val androidModule = module {
    single { this }
    single {
        RoomRepository(HotelDatabase.getDatabase(context = get()))as HotelRepository
    }
    viewModel {
        HotelListViewModel(repository = get())
    }
    viewModel {
        HotelDetailsViewModel(repository = get())
    }
    viewModel {
        HotelFormViewModel(repository = get())
    }
}

